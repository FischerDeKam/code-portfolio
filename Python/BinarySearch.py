#Fischer DeKam
#10/21/2015
#Binary Search for list of names

import math

names = ["Tom", "Jimmy", "Bob", "Billy", "Jack", "Phil", "Roger", "Matt", "Du", "Jim", "Sammy", "Jenni", "Todd", "Allie", "Jackie"]
names.sort()

imin = 0
imax = len(names)
found = False
finished = False
name = input("What name would you like to search for?: ")

while(not found and not finished):
    index = int(math.floor((imin + imax)/2))
    print (" %d, %d, %d" % (imin, imax, index))
    if (name == names[index]):
        found = True
    if (imin == index):
        finished = True
    elif (name > names[index]):
        imin = index
    else:
        imax = index


if (found):
    print (name + " is in the list at position " + str(index))
else:
    print(name + " is not in the list")
wait = input("End Program: Press ENTER to close")
print (wait)
