def fibb(term):
    if term > 2:
        result = fibb(term-1) + fibb(term-2)
    elif term == 2:
        result = 1
    elif term ==1:
        result = 0
    return result

value = int(input("Enter a positive whole number: "))
print ("The " + str(value) + "th fibbonacci number is " + str(fibb(value)))
