import math

#def means making a new function
def MechanicCost(hours, divs, flatfee, divfee):
    time = math.ceil(hours*divs)
    cost = time*divfee + flatfee
    return cost
print ("*****************************")
print ("*Cost of Mechanic Calculator*")
print ("*****************************")
hours = float(input("Enter a number of hours: "))

#notice how the spaces after function all line up
#hours = hours, divs = 2, flatfee = 40, and divfee = 20

ma = MechanicCost(hours, 2, 40, 20)
mb = MechanicCost(hours, 4, 24.5, 11.5)

if ma < mb:
    print ("Mechanic A is cheeper, at $" + str(ma))
elif ma > mb:
    print ("Mechanic B is cheeper, at $" + str(mb))
elif ma == mb:
    print ("Both cost the same")
