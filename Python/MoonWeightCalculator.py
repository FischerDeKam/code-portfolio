#By Fischer DeKam
#August 31st 2015
#CS 101
#To convert Earth weight to Moon weight

print ("This program converts Earth weight into Moon weight\n")

weight = float(input("Enter a weight on Earth in Kilograms: "))
#float allows decimal numbers
print ("That weight of that object on the moon is " + str(1/6*weight) + " Kilograms")
#str adds the variable into the string                
