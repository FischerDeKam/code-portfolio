def factorial(term):
    if (term >0):
        result = term * factorial(term-1)
    elif (term == 0):
        result = 1
    else:
        pass
    return result

value = int(input("Enter a positive whole number: "))
print(str(value) + "! = " + str(factorial(value)))

#below is just for extra information
total = 1
for i in range(1, value+1):
    total*=i
print (total)
