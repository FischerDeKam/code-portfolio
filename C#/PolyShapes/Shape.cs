using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolyShapes
{
    abstract class Shape
    {
        public int Radius { get; set; }

        public virtual void Draw() { }

    }
}
