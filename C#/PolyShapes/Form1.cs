using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PolyShapes
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Square square = new Square();
            square.Radius = 3;

            Circle circle = new Circle();
            circle.Radius = 4;

            List<Shape> shapes = new List<Shape>();
            shapes.Add(circle);
            shapes.Add(square);

            foreach (Shape i in shapes){
                i.Draw();
            }
        }
    }
}
