using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace KanyeREST
{
    class Api
    {
        public string inputTrack { get; set; }

        public Track getTrack()
        {
        inputTrack = inputTrack.ToLower();
        inputTrack = inputTrack.Replace(" ", "_");

            Track track = new Track();
        track.title = string.Format("http://www.kanyerest.xyz/api/track/{0}", inputTrack);
        
        WebClient client = new WebClient();
            try
            {

                String response = client.DownloadString(track.title);
                Track info = JsonConvert.DeserializeObject<Track>(response);

                return info;
            }
            catch
            {
                return null;
            }
        }
    }
   
}
