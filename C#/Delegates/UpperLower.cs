using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


delegate string UserInput(string input);

namespace Delegates
{
    class UpperLower 
    {
        public string input { get; set; }

        public string Upper(string input)
        {
            return input.ToUpper();
        }
        public string Lower(string input)
        {
            return input.ToLower();
        }
        public string ConvertUp()
        {
        UserInput upper = new UserInput(Upper);
            return upper(input);
        }
        public string ConvertDown()
        {
            UserInput lower = new UserInput(Lower);
            return lower(input);
        }

    }
}
