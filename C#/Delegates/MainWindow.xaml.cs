using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Delegates
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Upper_Click(object sender, RoutedEventArgs e)
        {
            string input = TextBox.Text;
            UpperLower upperlower = new UpperLower();
            upperlower.input = input;
            TextBox.Text = upperlower.ConvertUp();
        }

        private void Lower_Click(object sender, RoutedEventArgs e)
        {
            string input = TextBox.Text;
            UpperLower upperlower = new UpperLower();
            upperlower.input = input;
            TextBox.Text = upperlower.ConvertDown();
        }
    }
}
