namespace MessageCleaner
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Message = new System.Windows.Forms.TextBox();
            this.BannedWords = new System.Windows.Forms.TextBox();
            this.CleanedMessage = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Clean = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(0, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Message to be cleaned:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(330, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "List of banned words:";
            // 
            // Message
            // 
            this.Message.Location = new System.Drawing.Point(3, 62);
            this.Message.Multiline = true;
            this.Message.Name = "Message";
            this.Message.Size = new System.Drawing.Size(281, 300);
            this.Message.TabIndex = 2;
            // 
            // BannedWords
            // 
            this.BannedWords.Location = new System.Drawing.Point(333, 62);
            this.BannedWords.Multiline = true;
            this.BannedWords.Name = "BannedWords";
            this.BannedWords.Size = new System.Drawing.Size(278, 300);
            this.BannedWords.TabIndex = 3;
            // 
            // CleanedMessage
            // 
            this.CleanedMessage.Location = new System.Drawing.Point(651, 62);
            this.CleanedMessage.Multiline = true;
            this.CleanedMessage.Name = "CleanedMessage";
            this.CleanedMessage.Size = new System.Drawing.Size(282, 300);
            this.CleanedMessage.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(648, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Cleaned message:";
            // 
            // Clean
            // 
            this.Clean.Location = new System.Drawing.Point(416, 394);
            this.Clean.Name = "Clean";
            this.Clean.Size = new System.Drawing.Size(125, 23);
            this.Clean.TabIndex = 6;
            this.Clean.Text = "Clean Message";
            this.Clean.UseVisualStyleBackColor = true;
            this.Clean.Click += new System.EventHandler(this.Clean_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(939, 464);
            this.Controls.Add(this.Clean);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.CleanedMessage);
            this.Controls.Add(this.BannedWords);
            this.Controls.Add(this.Message);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Message Cleaner";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Message;
        private System.Windows.Forms.TextBox BannedWords;
        private System.Windows.Forms.TextBox CleanedMessage;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button Clean;
    }
}

