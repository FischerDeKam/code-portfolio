using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessageCleaner
{
    class Cleaner
    {
        public string[] MessageArray { get; set; }
        public string[] BannedArray { get; set; }
        public string Censored { get; set; }

        public string CleanMessage(string[] MessageArray, string[] BannedArray)
        {
            for (int i = 0; i < MessageArray.Count(); i++)
            {
                for (int j = 0; j < BannedArray.Count(); j++)
                {
                    if (MessageArray[i] == BannedArray[j])
                    {
                        MessageArray[i] = "****";                       
                    }
                }
            }
           
                 

            Censored = string.Join(" ", MessageArray);
            return Censored;
        }
    }
}
