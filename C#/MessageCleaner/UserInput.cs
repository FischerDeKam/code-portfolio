using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessageCleaner
{
    class UserInput
    {
        public string InputMessage { get; set; }
        public string InputBannedWords { get; set; }       
    }
}
