using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MessageCleaner
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Clean_Click(object sender, EventArgs e)
        {
            string inputMessage = Message.Text.ToLower();
            string inputBannedWords = BannedWords.Text.ToLower();

            UserInput userInput = new UserInput();
            userInput.InputMessage = inputMessage;
            userInput.InputBannedWords = inputBannedWords;

            Cleaner cleaner = new Cleaner();
            cleaner.MessageArray = userInput.InputMessage.Split(' ', ',', '.');
            cleaner.BannedArray = userInput.InputBannedWords.Split(' ');
            //string final = cleaner.CleanMessage(cleaner.MessageArray, cleaner.BannedArray);

            //CleanedMessage.Text = final;
            CleanedMessage.Text = cleaner.CleanMessage(cleaner.MessageArray, cleaner.BannedArray);
            
        }
    }
}
