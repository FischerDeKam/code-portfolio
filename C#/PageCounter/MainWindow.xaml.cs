using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PageCounter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void CreateStudent_Click(object sender, RoutedEventArgs e)
        {
            string inputName = Name.Text;
            string inputBear = BearNumber.Text;

            Student student = new Student();
            student.BearNumber = inputBear;
            student.Name = inputName;

            Console.WriteLine(student.Name);
        }

        private void CreateBook_Click(object sender, RoutedEventArgs e)
        {
            string inputTitle = Title.Text;
            string inputPages = Pages.Text;
            string inputIsbn = Isbn.Text;

            Textbook book = new Textbook();
            book.Title = inputTitle;
            book.PageNumber = Convert.ToInt32(inputPages);
            book.Isbn = inputIsbn;
        }
    }
}
