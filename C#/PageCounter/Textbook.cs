using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PageCounter
{
    class Textbook
    {
        public string Title { get; set; }

        public int PageNumber { get; set; }

        public string Isbn { get; set; }
    }
}
