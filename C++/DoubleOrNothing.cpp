//Created by Fischer DeKam
//This is a game that allows the player to bet money on heads or tails, while keeping track of their total in the bank

#include <iostream>
#include <ctime> //for time()
#include <cstdlib> //for srand() and rand()
using namespace std;

static int calculate(int wager, char prediction) {
	srand(time(0)); //Initialize random number generator
	int r = (rand() % 1000) + 1;
	int remainder = r % 2; //Flip coin

	int moneyexchanged;

	if (remainder == 0) {
		//Coin flip is heads
		switch (prediction) {
		case 'h': //Player bet on heads
			cout << "\nThe coin flip was: Heads\n";
			cout << "You win!\n";
			moneyexchanged = wager;
			cout << moneyexchanged << " has been added to your bank.\n";
			cout << "-----------------------------------\n";
			break;
		case 't': //Player bet on tails
			cout << "\nThe coin flip was: Heads\n";
			cout << "You lose\n";
			moneyexchanged = 0 - wager;
			cout << moneyexchanged << " has been deducted from your bank.\n";
			cout << "-----------------------------------\n";
			break;
		}
	}
	else {
		//Coin flip is tails
		switch (prediction) {
		case 'h': //Player bed on heads
			cout << "\nThe coin flip was: Tails\n";
			cout << "You lose\n";
			moneyexchanged = 0 - wager;
			cout << moneyexchanged << " has been deducted from your bank.\n";
			cout << "-----------------------------------\n";
			break;
		case 't': //Player bet on tails
			cout << "\nThe coin flip was: Tails\n";
			cout << "You win!\n";
			moneyexchanged = wager;
			cout << moneyexchanged << " has been added to your bank.\n";
			cout << "-----------------------------------\n";
			break;
		}
	}
	return moneyexchanged;
}

int main() {
//Display instructions
	cout << "+---------------------------------+\n";
	cout << "|  Welcome to Double or Nothing!  |\n";
	cout << "|    Created by Fischer DeKam     |\n";
	cout << "+---------------------------------+\n";
	int bank = 100;

	while (bank > 0) {
		cout << "\nBank balance: " << bank << endl;
	//Player Bets
		int wager;
		cout << "\nHow much would you like to bet?\n";
		cin >> wager;
		while (wager > bank) {
			cout << "Please enter a valid amount\n";
			cin >> wager;
		}
		
		char prediction;
		cout << "\nHeads or Tails? (h/t): \n";
		cin >> prediction;

		bank += calculate(wager, prediction);//Result calculated, Payment (to or from player)
	}
	if (bank <= 0){
	cout << "\n+---------------------------------+\n";
	cout << "|      Thank you for playing!     |\n";
	cout << "|           Try again             |\n";
	cout << "+---------------------------------+\n\n";
	}
	return 0;
}