// BaccaratSimulator.cpp : This file contains the 'main' function. Program execution begins and ends there.
// Created by Fischer DeKam

#include <iostream>
#include <ctime> //for time()
#include <cstdlib> //for srand() and rand()
using namespace std;

//Shuffle cards function
void shuffle(int card[], int n) {
	srand(time(0)); //Initialize random number generator

	for (int i = 0; i < n; i++) {
		int random = i + (rand() % (416 - i)); //Random array index to swap with the remaining cards
		swap(card[i], card[random]);
	}
}

//Draw function
void drawCard(int hand[], int shoe[], int h, int n) {
	int HandPosition = h;
	int CardToDraw = n;
	hand[HandPosition] = shoe[CardToDraw];
}

//Display hand
void displayHand(int hand[]) {
	for (int i = 0; i < 3; i++) {
		switch (hand[i]) {
		case 1:
			cout << 'A' << ",";
			break;
		case 11:
			cout << 'J' << ",";
			break;
		case 12:
			cout << 'Q' << ",";
			break;
		case 13:
			cout << 'K' << ",";
			break;
		case 0:
			cout << ' ';
			break;
		default:
			cout << hand[i] << ",";
			break;
		}
	}
}

//Calculate value
static int calculateValue(int hand[]) {
	int handSum = 0;
	for (int i = 0; i < 3; i++) {
		switch (hand[i]) {
		case 10:
			handSum += 0;
			break;
		case 11:
			handSum += 0;
			break;
		case 12:
			handSum += 0;
			break;
		case 13:
			handSum += 0;
			break;
		default:
			handSum += hand[i];
			break;
		}
	}
	return handSum;
}

int main()
{
	cout << "Welcome to the Punto Banco Simulator!\n\n";

	//Array of 8 decks of cards
	int deck[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
			1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
			1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
			1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
			1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
			1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
			1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
			1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
			1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
			1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
			1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
			1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
			1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
			1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
			1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
			1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
			1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
			1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
			1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
			1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
			1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
			1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
			1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
			1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
			1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
			1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
			1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
			1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
			1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
			1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
			1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
			1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };

	//Shuffle the deck of card values
	shuffle(deck, 416);

	//Initialize Bank hand and Player hand
	int PHand[3] = { 0, 0, 0 };
	int BHand[3] = { 0, 0, 0 };

	//Deal cards to player and bank
	drawCard(PHand, deck, 0, 0);
	drawCard(BHand, deck, 0, 1);
	drawCard(PHand, deck, 1, 2);
	drawCard(BHand, deck, 1, 3);

	//Calculate initial hand values
	int PHandSum = calculateValue(PHand);
	if (PHandSum >= 10) {
		PHandSum -= 10;
	}
	int BHandSum = calculateValue(BHand);
	if (BHandSum >= 10) {
		BHandSum -= 10;
	}

	//Drawing rules
	if (PHandSum >= 8 || BHandSum >= 8) {
		PHand[2] = 0;
		BHand[2] = 0;
	}
	else if (PHandSum <= 5) {
		drawCard(PHand, deck, 2, 4);
		switch (BHandSum) {
		case 0:
			drawCard(BHand, deck, 2, 5);
			break;
		case 1:
			drawCard(BHand, deck, 2, 5);
			break;
		case 2:
			drawCard(BHand, deck, 2, 5);
			break;
		case 3:
			if (PHand[2] != 8) {
				drawCard(BHand, deck, 2, 5);
			}
			else {
				BHand[2] = 0;
			}
			break;
		case 4:
			if (PHand[2] == 2 || PHand[2] == 3 || PHand[2] == 4 || PHand[2] == 5 || PHand[2] == 6 || PHand[2] == 7) {
				drawCard(BHand, deck, 2, 5);
			}
			else {
				BHand[2] = 0;
			}
			break;
		case 5:
			if (PHand[2] == 4 || PHand[2] == 5 || PHand[2] == 6 || PHand[2] == 7) {
				drawCard(BHand, deck, 2, 5);
			}
			else {
				BHand[2] = 0;
			}
			break;
		case 6:
			if (PHand[2] == 6 || PHand[2] == 7) {
				drawCard(BHand, deck, 2, 5);
			}
			else {
				BHand[2] = 0;
			}
			break;
		case 7:
			BHand[2] = 0;
			break;
		}
	}
	else if (BHandSum <= 5) {
		drawCard(BHand, deck, 2, 4);
	}

	//Display hand after the drawing rules are applied
	cout << "Players hand: ";
	displayHand(PHand);
	cout << endl;
	cout << "Banks hand: ";
	displayHand(BHand);
	cout << endl;

	//Calculate hand value after drawing rules are applied
	PHandSum = calculateValue(PHand);
	while (PHandSum >= 10) {
		PHandSum -= 10;
	}
	BHandSum = calculateValue(BHand);
	while (BHandSum >= 10) {
		BHandSum -= 10;
	}

	//Determine winner
	if (PHandSum > BHandSum) {
		cout << "PLAYER\n";
	}
	else if (BHandSum > PHandSum) {
		cout << "BANK\n";
	}
	else if (PHandSum = BHandSum) {
		cout << "TIE\n";
	}

	//End of main
	return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu
