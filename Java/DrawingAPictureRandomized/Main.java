/*****************************************************************************
 * 									Main
 * 										
 * Programmed by: Fischer DeKam (September 16, 2016)
 * Class: CS200
 * Assignment 3
 * Instructor: Dean Zeller
 *
 * Description: Main tests shapes, and draws them
 *****************************************************************************/
import javax.swing.JApplet;
import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

public class Main extends JApplet{

	public Color RandomColor() {

		Color RColor = null;
		Random ran = new Random();

		int number = ran.nextInt(9) + 1;

		switch (number) {
		case 1:
			RColor = Color.RED.darker();
			break;
		case 2:
			RColor = Color.RED;
			break;
		case 3:
			RColor = Color.YELLOW;
			break;
		case 4:
			RColor = Color.GREEN;
			break;
		case 5:
			RColor = Color.blue;
			break;
		case 6:
			RColor = Color.cyan;
			break;
		case 7:
			RColor = Color.MAGENTA;
			break;
		case 8:
			RColor = Color.PINK;
			break;
		case 9:
			RColor = Color.BLUE.darker();
			break;
		default:
			RColor = Color.white;
			break;
		}
		return RColor;
	}



	public void paint(Graphics canvas) {
		for (int i=0; i<480; i+=120)
		for (int j=0; j<360; j+=120){


			Square s1 = new Square(25+i, 50+j, 100, RandomColor());
			Square s2 = new Square(30+i, 60+j, 20, RandomColor());
			Square s3 = new Square(80+i, 60+j, 20, RandomColor());
			Rectangle r1 = new Rectangle(60+i, 100+j, 25, 50, RandomColor());
			Triangle t1 = new Triangle(75+i, 50+j, 100, 50, RandomColor());
			s1.paint(canvas);
			r1.paint(canvas);
			t1.paint(canvas);
			s2.paint(canvas);
			s3.paint(canvas);

			//s1.printInfo();
			//r1.printInfo();
			//t1.printInfo();
		}
	}

}