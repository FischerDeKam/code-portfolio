//Fischer DeKam
//Lab 5     CS 102
//This program converts Roman Numerals inputs into Arabic Numbers
import java.util.Scanner;

public class Lab_5 {

	public static void main(String[] args) {
		int again = 0;	
		Scanner kb = new Scanner(System.in);
		while (again == 0){
			
			System.out.println("Enter a Roman Numeral value in all caps: ");
			String in = kb.next();
			int result  = roman(in);	
			System.out.print(result);
			System.out.println("\nWould you like to convert another number? (y/n)");
			String X = kb.next();
			if (X.equalsIgnoreCase("n")){
				again += 1;
			}
			else if (X.equalsIgnoreCase("y")){
				again += 0;
			}
				
		}kb.close();
	}


	public static int roman(String input)
	{
		int total = 0;
		for (int i = 0; i < input.length(); i++)
		{
			char convert = input.charAt(i);
			switch (convert)
			{
			case 'I':
				total += 1;
				break;
			case 'V':
				total += 5;
				break;
			case 'X':
				total += 10;
				break;
			case 'L':
				total += 50;
				break;
			case 'C':
				total += 100;
				break;
			case 'D':
				total += 500;
				break;
			case 'M':
				total += 1000;
				break;
			}

		}
		if (input.contains("IV")){
			total -= 2;
		}
		if (input.contains("IX")){
			total -= 2;
		}
		if (input.contains("XL")){
			total -= 20;
		}
		if (input.contains("XC")){
			total -= 20;
		}
		if (input.contains("CD")){
			total -= 200;
		}
		if (input.contains("CM")){
			total -= 200;
		}
		return total;
	}

}