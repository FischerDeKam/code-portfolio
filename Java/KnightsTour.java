import java.util.Scanner;
public class Tour {

	public static final int size = 8;
	public static final int[][] priority = {
			{2,3,4,4,4,4,3,2},
			{3,4,6,6,6,6,4,3},
			{4,6,8,8,8,8,6,4},
			{4,6,8,8,8,8,6,4},
			{4,6,8,8,8,8,6,4},
			{4,6,8,8,8,8,6,4},
			{3,4,6,6,6,6,4,3},
			{2,3,4,4,4,4,3,2}};
	
	
	public static void main(String[] args) {
		if(!boardIsFull(new int[8][8]))
			displayBoard(new int[8][8]);
		int board[][] = new int[8][8];
		Scanner kb = new Scanner(System.in);
		System.out.println("\n\nWhich row and col should the Knight start: (1-8)");
		int row = kb.nextInt()-1, col = kb.nextInt()-1;
		board [row][col] = 1;
		moveKnight(row, col, 2, board);
		displayBoard(board);
		kb.close();

	}
	/*public static int[][] moveOrder(int row, int col){
		int[][] moves = {{-2, 1, 0},{-1, 2, 0},{1, 2, 0},{2, 1, 0},{2, -1, 0},{1, -2, 0},{-1, -2, 0},{-2, -1, 0}};
		
		for (int[] move : moves){
			try{
				move[2] = priority[row +move[0]][col + move[1]];
			}
			catch (Exception e){
				move[2] = 9;
			}
			
		}
		for (int i=0; i<size; i++){
			int smallest = i;
			for(int j = i; j<size; j++){
				if (moves[smallest][2] > moves[j][2]){
					smallest = j;
				}
			}
			int[] temp = moves[smallest];
			moves[smallest] = moves[i];
			moves[i] = temp;
		}
		return moves;
	}*/
	
	
	public static boolean moveKnight(int row, int col, int turn, int[][] board){
		int moves[][] = {{-2, 1}, {-1, 2}, {1, 2}, {2, 1}, {2, -1}, {1, -2}, {-1, -2}, {-2, -1}};
		boolean success = false;
		for(int[] move : moves){
			try{
				if(board[row+move[0]][col+move[1]]==0){
					board[row+move[0]][col+move[1]] = turn;
					success = moveKnight(row+move[0], col+move[1], turn+1, board);
					if(!success){
						board[row+move[0]][col+move[1]] = 0;
					}
				}
			}
			catch (Exception e){
			}
			if (success)
				break;
		}
		if(!success && boardIsFull(board))
			success = true;
		return success;
	}

	public static boolean boardIsFull(int[][] board){
		boolean full = true;
		for (int[] row : board){
			for (int col : row){
				if (col == 0){
					full = false;
					break;
				}
			}
		}
		return full;
	}

	public static void displayBoard(int[][] board){
		System.out.println(" _ _ _ _ _ _ _ _");
		for (int[] row : board){
			for (int col : row){
				System.out.print("|"+col);
			}
			System.out.println("|");
		}
	}

}
