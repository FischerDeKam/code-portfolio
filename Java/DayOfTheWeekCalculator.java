//Fischer DeKam
import java.util.Scanner;
public class Problem_1 {

	public static void main(String[] args) {
		Scanner kb = new Scanner(System.in);

		System.out.println("Enter A Month (1-12): ");  //Month 1-12
		int m = kb.nextInt();
		System.out.println("Enter the Day (0-6): ");  //Day 0-6, Sunday = 0, Mon = 1, etc.
		int d = kb.nextInt();
		System.out.println("Enter the Year (yyyy): ");  //Year
		int y = kb.nextInt();

		kb.close();

		int year = y-((14-m)/12);  //Formulas
		int x = year + (y/4)-(y/100)+(y/400);
		int month = m+12*((14-m)/12)-2;
		int day = (d+x+((31*month)/12))%7;

		System.out.println("The day of the week is "+ day);

	}
}