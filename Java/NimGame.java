//Fischer DeKam
//Nim
import java.util.Scanner;
import java.util.Random;
public class Game {

	public static void main(String[] args) {
		int again = 0;
		Scanner kb = new Scanner(System.in);
		System.out.println("The Game of Nim");	
		int sticks = 15;
		while (again == 0){	
			do{
				int board = board(sticks);
				System.out.print(board);	
				checkWinner(sticks);
				sticks -= playerTurn(sticks);
				checkWinner(sticks);
				if (sticks <= 0){
					System.out.println("PC Wins!");
				}
				else{
					sticks -= compTurn(sticks);
					checkWinner(sticks);
					if (sticks <= 0){
						System.out.println("You Win!");
					}
				}
			}while (sticks > 0);
			System.out.println("Would you like to play again? (y/n)");
			String X = kb.next();
			if (X.equalsIgnoreCase("y")){
				again += 0;
				sticks = 15;
			}
			if (X.equalsIgnoreCase("n")){
				again += 1;
				System.out.println("End Program");
			}
		}
	}
	public static int playerTurn(int sticks) {
		Scanner kb = new Scanner(System.in);
		System.out.println("\nHow many sticks would you like to pick up? (1, 2, 3)");
		int playerNumber = kb.nextInt();  //How many sticks the player picks up
		//Takes that amount of sticks out of the remaining total of sticks
		if (playerNumber < 0 || playerNumber > 3){
			System.out.println("Please enter 1, 2, or 3");
			playerNumber = kb.nextInt();
		}
		return playerNumber;
	}

	public static int compTurn(int sticks) {
		Random gen = new Random();
		int compNumber = gen.nextInt(3);  // generates a number between 0 and 2
		compNumber += 1;  //Then adds 1, so only possible to get 1, 2, and 3
		System.out.println("Computer takes " + compNumber + " sticks.");  //tells the user what the PC did
		return compNumber;
	}

	public static void checkWinner(int sticks){
		if (sticks <= 0){
			System.out.println("Game over");
		}
		return ;
	}

	public static int board(int sticks) {  //Displays a visual of how many sticks remain
		for (int i = 0; i < sticks; i++){
			System.out.print("| ");
		}
		return sticks;
	}
}