//Fischer DeKam
//Lab 7
import java.util.Scanner;
public class Main_Program {

	public static void main(String[] args) {

		Scanner kb = new Scanner(System.in);
		System.out.println("This program evaluates an expression of two fractions added or subtracted");
		System.out.println("The expression should look like this: A/B + C/D "
				+ "\nwhere A, B, C, and D are intigers, the + sign can be a - sign, and spacing is important");
		System.out.println("Enter the expression: ");
		final String num1, num2, den1, den2;
		String input = kb.nextLine();
		kb.close();
		String delims = "[-+/ ]+";
		String[] tokens = input.split(delims);	

		num1 = tokens[0];
		den1 = tokens[1];
		num2 = tokens[2];
		den2 = tokens[3];

		String fractionA = (num1 +"/"+ den1);
		String fractionB = (num2 +"/"+ den2);

		Integer A = Integer.valueOf(num1);
		Integer B = Integer.valueOf(den1);
		Integer C = Integer.valueOf(num2);
		Integer D = Integer.valueOf(den2);

		if (input.contains("+")){
			int	newNum = (A*D)+(C*B);
			int newDen = (B*D);
			String newFrac = (newNum  +"/"+ newDen);
			System.out.print(fractionA +" + "+ fractionB +" = "+ newFrac);
		}
		if (input.contains("-")){
			int	newNum = (A*D)-(C*B);
			int newDen = (B*D);
			String newFrac = (newNum +"/"+ newDen);
			System.out.print(fractionA +" - "+ fractionB +" = "+ newFrac);
		}
		System.out.print("\nThank you for using the program");
	}
}
