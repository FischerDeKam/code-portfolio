//Fischer DeKam
//Lab 7
public class Fraction {

	private int numerator;
	private int denominator;


	Fraction(int n, int d){
		numerator = n;
		denominator = d;
	}

	Fraction add(Fraction second){
		int newNumerator = (numerator*second.denominator)+(second.numerator*denominator);
		int newDenominator = denominator*second.denominator;
		return new Fraction (newNumerator, newDenominator);
	}
	Fraction subtract(Fraction second){
		int newNumerator = (numerator*second.denominator)-(second.numerator*denominator);
		int newDenominator = denominator*second.denominator;
		return new Fraction (newNumerator, newDenominator);
	}
	public String toString(){
		int n = numerator;
		int d = denominator;
		return(n+"/"+d);
	}
}
