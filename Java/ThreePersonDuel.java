//Fischer DeKam  February 3, 2016
//Lab 3
//CS 102
//"Duel between three men. Aaron has 1/3 accuracy, Bob has 1/2 accuracy, and Charlie has perfect accuracy.
//Each duelist will aim for the duelist with the highest accuracy."
import java.util.Random;
public class Lab_3 {

	public static void main(String[] args) {
	System.out.println("This program simulates a single duel between 3 participants");
//Health Status
	boolean AaronAlive = true;
	boolean BobAlive = true;
	boolean CharlieAlive = true;
	
	int Living = 3; //How many people are alive to participate in duel
	Random gen = new Random();

while (Living > 1){
//Aaron's turn	
if (AaronAlive){
	double shot = gen.nextDouble();
	double AaronShot = (shot*3);
	if (AaronShot <= 1){
		Living -= 1; //After a kill, number of participants decreases by one
		if (CharlieAlive){
			CharlieAlive = false;
			System.out.println("Aaron kills Charlie");}
		else if (BobAlive){
			BobAlive = false;
			System.out.println("Aaron kills Bob");}
	}
	else
	{
	//System.out.println("Aaron misses"); //For testing purposes, use this line to check for missed shots
	}
}
//Bob's turn
if (BobAlive){
	double shot = gen.nextDouble();
	double BobShot = (shot*2);
	if (BobShot <= 1){
		Living -= 1;
		if (CharlieAlive){
			CharlieAlive = false;
			System.out.println("Bob kills Charlie");}
		else if (AaronAlive){
			AaronAlive = false;
			System.out.println("Bob kills Aaron");}
	}
	else
	{
	//System.out.println("Bob misses"); //For testing purposes, use this line to check for missed shots
	}
}
//Charlie's turn
if (CharlieAlive){
	if (BobAlive){
		BobAlive = false;
		Living -= 1;
		System.out.println("Charlie kills Bob");}
	else
	{
		AaronAlive = false;
		Living -= 1;
		System.out.println("Charlie kills Aaron");}
	}
}		
	if (AaronAlive){
		System.out.println("Aaron wins the duel");}
	if (BobAlive){
		System.out.println("Bob wins the duel");}
	if (CharlieAlive){
		System.out.println("Charlie wins the duel");}
	
	}
}
