//Fischer DeKam
import java.util.Scanner;
public class In_Class_5 {
	
	public static void main(String[] args) {
		
	Scanner kb = new Scanner(System.in);
	System.out.println("This program will output the Circumference, Area, and Volume of a sphere");
	System.out.println("Enter the radius: ");
	double rad = kb.nextDouble();
	double circ = ((2)*(Math.PI)*(rad));
	double area = ((4)*(Math.PI)*(Math.pow(rad, 2)));
	double vol = ((4/3)*(Math.PI)*(Math.pow(rad, 3)));

    System.out.printf("The follwing are properties for a sphere of radius %f: %n", rad);
    System.out.printf("Circumference = %f%n",circ);
    System.out.printf("Area = %f%n", area);
    System.out.printf("Volume = %f", vol);
    kb.close();
	}
}
