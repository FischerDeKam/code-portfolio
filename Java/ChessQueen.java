//Fischer DeKam
import java.util.Scanner;
public class Queen {
	public static final int size = 8;

	public static void main(String[] args) {
		if(!boardIsFull(new int[size][size]))
			displayBoard(new int[size][size]);
		int board[][] = new int[size][size];
		Scanner kb = new Scanner(System.in);
		System.out.println("\n\nWhich row in the first column should the fisrt Queen be placed?: (1-8)");
		int row = kb.nextInt()-1, col = 0;
		board [row][col] = (2);
		attackBoard(row, col, board);
		displayBoard(board);

		kb.close();

	}
	private static void displayBoard(int[][] board){  //displays the board, using a 2D array
		System.out.println(" _ _ _ _ _ _ _ _");
		for (int[] row : board){
			for (int col : row){
				System.out.print("|"+col);
			}
			System.out.println("|");
		}
	}

	private static boolean boardIsFull(int[][] board){  //checks to see when program should end
		boolean full = true;
		for (int[] row : board){
			for (int col : row){
				if (col == 0){
					full = false;
					break;
				}
			}
		}
		return full;
	}

	private static boolean attackBoard(int row, int col, int[][] board){  //Queen on board appears as "8", attack paths as "+1"
		boolean success = false;
		for (int i=0; i<size; i++){
			board[i][col] += 1;  //Horizontal
			board[row][i] += 1;  //Vertical 
		}
		for (int i=0; i<size; i++){  //Diagonal 
			if (row+i > 7 || col+i > 7)
				break;
			board[row+i][col+i] += 1;
		}
		for (int i=0; i<size; i++){
			if (row-i < 0 || col+i > 7)
				break;
			board[row-i][col+i] += 1;
		}
		for (int i=0; i<size; i++){
			if (row+i > 7 || col-i < 0)
				break;
			board[row+i][col-i] += 1;
		}
		for (int i=0; i<size; i++){
			if (row-i < 0 || col-i < 0)
				break;
			board[row-i][col-i] += 1;
		}
//*******Placing Additional Queens**************
		for (col = 1; col < 8; col++){  //Starts on the second column

			for (row = 0; row < 8; row++){  //Goes from row 1 through 8  

				if (board[row][col] == 0){  //looks for a "0", then places a Queen and its attacks
					board[row][col] = 2;	

					for (int i=0; i<size; i++){
						board[i][col] += 1;  //Horizontal
						board[row][i] += 1;  //Vertical 
					}
					for (int i=0; i<size; i++){  //Diagonal 
						if (row+i > 7 || col+i > 7)
							break;
						board[row+i][col+i] += 1;
					}
					for (int i=0; i<size; i++){
						if (row-i < 0 || col+i > 7)
							break;
						board[row-i][col+i] += 1;
					}
					for (int i=0; i<size; i++){
						if (row+i > 7 || col-i < 0)
							break;
						board[row+i][col-i] += 1;
					}
					for (int i=0; i<size; i++){
						if (row-i < 0 || col-i < 0)
							break;
						board[row-i][col-i] += 1;
					}
				}
			}
		}
		return success;
	}	
}