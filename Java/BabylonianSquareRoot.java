//Fischer DeKam
//Lab 1
//Due Jan 22, 2015
import java.util.Scanner;
public class L1 {

	public static void main(String[] args) {
		System.out.println("This program finds the square root of a number using the Babylonian algorithm");
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Enter a number to find the square root of: ");
		double number = keyboard.nextDouble();
//First Iteration
		double guess = (number / 2);
		double sqroot = (number / guess);
//Second Iteration
		double guessI = ((guess + sqroot)/2);
		double sqrootI = (number / guessI);
//Third Iteration
		double guessII = ((guessI + sqrootI)/2);
		double sqrootII = (number / guessII);
//Fourth Iteration
		double guessIII = ((guessII + sqrootII)/2);
		double sqrootIII = (number / guessIII);
//Fifth Iteration
		double guessIV = ((guessIII + sqrootIII)/2);
		double sqrootIV = (number / guessIV);
		
		System.out.printf("The square root of %.1f determined by the Babylonian algorithm is: %.4f \n", number, sqrootIV);
		
		double actsqroot = (Math.sqrt(number));
		System.out.printf("The actual square root from the math library is: %.1f", actsqroot);
		keyboard.close();
	}

}
