//Fischer DeKam
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
public class Problem_2 {

	public static void main(String[] args) {
		String text = getText();
		System.out.println(text);
		String newText = text.toLowerCase();
		System.out.print(newText);
	}

	private static String getText(){  //File input from computer
		String text = "";
		Scanner kb = new Scanner(System.in);
		System.out.println("Enter the name to check for palindrome: "); //pal1.txt or pal2.txt
		String fileName = kb.nextLine();
		Scanner fileIn = null;

		try {
			fileIn = new Scanner(new File(fileName));
		}
		catch (FileNotFoundException e){
			e.printStackTrace();
			System.exit(0);
		}
		while (fileIn.hasNextLine()){
			text += fileIn.nextLine();
		}
		fileIn.close();
		kb.close();
		return text;
	}

	private String pal(String text){  //Palendome Check
		for (int i=0; i <= text.length(); i++){
			if (text.charAt(0) == text.charAt(text.length())){  //Return true if palindrome
				return "true";
			}
			if (text.charAt(i+1) == text.charAt(text.length())){
				return "true";
			}
			else
				return "false";  //return false if not palindrome
		}

		return text;

	}
}