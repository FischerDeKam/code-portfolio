//Fischer DeKam
//This program calculates amount of change to be given back to the customer, and tells the cashier what coins to give back
import java.util.Scanner;
public class Problem_1 {

	public static void main(String[] args) {
		Scanner kb = new Scanner(System.in);
		int quarter = 0;  //Initialize all to Zero
		int dime = 0;
		int nickel = 0;
		int penny = 0;
		int total = 100;
		System.out.println("How much does the item cost in cents? (0-100)");
		int cost = kb.nextInt();
		int change = total - cost;
		do{	
			if (change >= 25){  //If remaining total is more than 25, adds 1 quantity to the quarters, and subtracts 25 cents from remaining change
				quarter += 1;
				change -= 25;
			}
			else if (change >= 10){  //More than 10, adds a dime, subtracts 10
				dime += 1;
				change -= 10;
			}
			else if (change >= 5){  //More than 5, adds nickel, subtracts 5
				nickel += 1;
				change -= 5;
			}
			else if (change >= 1){  //More than 1, adds penny, subtracts 1
				penny += 1;
				change -= 1;
			}  //Takes out as many quarters as possible, than dimes, than nickels, and pennies. Until there is no more change to be given back.
		}while (change > 0);	
		System.out.printf("The change is "+ (total - cost) + " cents:");  //Display what change to return
		System.out.print("\nQuarters: " + quarter);
		System.out.print("\nDimes : " + dime);
		System.out.print("\nNickels: " + nickel);
		System.out.print("\nPennies: " + penny);
		System.out.println(" ");
		System.out.println("\nEnd Program");
		kb.close();
	}
}