/*****************************************************************************
 * 									Main
 * 										
 * Programmed by: Fischer DeKam (September 14, 2016)
 * Class: CS200
 * Instructor: Dean Zeller
 *
 * Description: Main tests shapes, and draws them
 *****************************************************************************/
import javax.swing.JApplet;
import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

public class Main extends JApplet{

	Square s1 = new Square(25, 50, 100, Color.RED);
	Square s2 = new Square(30, 60, 20, Color.GREEN);
	Square s3 = new Square(80, 60, 20, Color.GREEN);
	Rectangle r1 = new Rectangle(60, 100, 25, 50, Color.BLUE);
	Triangle t1 = new Triangle(75, 50, 100, 50, Color.YELLOW);
	public void paint(Graphics canvas) {
		s1.paint(canvas);
		r1.paint(canvas);
		t1.paint(canvas);
		s2.paint(canvas);
		s3.paint(canvas);

		s1.printInfo();
		r1.printInfo();
		t1.printInfo();

	}

}

