/*****************************************************************************
 * 									Rectangle
 * 										
 * Programmed by: Fischer DeKam (September 14, 2016)
 * Class: CS200
 * Instructor: Dean Zeller
 *
 * Description: Rectangle is a class extending the Shape class, defining the 
 * properties of a rectangle.
 *****************************************************************************/
import java.awt.Color;
import java.awt.Graphics;
public class Rectangle extends Shapes{

	//Fields
	private double sideA;
	private double sideB;
	
	//Constructor
		public Rectangle(int x, int y, double sideA, double sideB, Color color){
			this.x = x;
			this.y = y;
			this.sideA = sideA;
			this.sideB = sideB;
			this.color = color;
		}
	
		public double getArea()	{return sideA*sideB;}
		public double getPerimeter()	{return (2*sideA)+(2*sideB);}
		public void printInfo(){
			System.out.println("Rectangle: "+"\n Coordinates: ("+getX()+","
					+getY()+")"+"\n Color: "+color+
					"\n Side Length A: "+sideA+
					"\n Side Length B: "+sideB+
					"\n Area: "+getArea()+
					"\n Perimeter: "+getPerimeter());
		}

	public void paint(Graphics canvas){
		canvas.setColor(color);
		canvas.fillRect(x,  y, (int)(sideA), (int)(sideB));
	}

}
