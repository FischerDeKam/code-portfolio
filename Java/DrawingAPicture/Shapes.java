/*****************************************************************************
 * 									Shapes
 * 										
 * Programmed by: Fischer DeKam (September 14, 2016)
 * Class: CS200
 * Instructor: Dean Zeller
 *
 * Description: Shapes is an abstract class that serves as the inherited base
 * for other shape classes, such as squares, circles, or pentagons.
 *****************************************************************************/
import java.awt.Color;
import java.awt.Graphics;

public abstract class Shapes{

	//Fields
	protected Color color; 
	protected int x, y;  //Cordinates of the shape
	
	//Abstract Methods
	public abstract double getArea();
	public abstract double getPerimeter();
	public abstract void printInfo();
	public abstract void paint(Graphics canvas);
	
	// Get and Set Methods
	public Color getColor()	{return color;}
	public void setColor(Color c)	{color = c;}
	public void setXY(int x, int y) {
		this.x = x;
		this.y = y;
	}
	public int getX()	{return x;}
	public int getY()	{return y;}

}
