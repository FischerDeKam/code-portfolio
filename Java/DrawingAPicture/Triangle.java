/*****************************************************************************
 * 									Triangle
 * 										
 * Programmed by: Fischer DeKam (September 14, 2016)
 * Class: CS200
 * Instructor: Dean Zeller
 *
 * Description: Triangle is a class extending the Shape class, defining the 
 * properties of a triangle.
 *****************************************************************************/
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;


public final class Triangle extends Shapes{
	//Fields
	private double sideLength;
	private int Base;
	private int Height;

	//Constructor
	public Triangle(int x, int y, int base, int height, Color color)
	{
		this.x = x;
		this.y = y;
		this.Height = height;
		this.Base = base;
		this.color = color;
	}

	//Define abstract methods from Shapes
	public double getArea()	{return ((Base*Height)/2);}
	public double getPerimeter() {return 3*Base;}
	public void printInfo(){
		System.out.println("Triangle: \n Color: "+getColor()+
				"\n SideLength: "+sideLength+ "\n Area: "
				+getArea()+ "\n Perimeter: "+ getPerimeter());
	}

	//Define methods for Triangle
	public double getSideLength() {return sideLength;}

	@Override
	public void paint(Graphics canvas) {
		int xPoly[] = {x-(Base/2),x+(Base/2),x};
		int yPoly[] = {y, y, (y-Height)};
		
		Polygon Triangle = new Polygon(xPoly, yPoly, 3);
		//Coordinates and number of sides
		
		canvas.setColor(color);
		canvas.fillPolygon(Triangle);
		

	}

}
