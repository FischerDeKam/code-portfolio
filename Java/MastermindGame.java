//Fischer DeKam
import java.util.Scanner;
import java.util.Random;

public class Game {
	public static void main(String[] args) {

		System.out.println("Welcome to Mastermind");
		Random gen = new Random();
		int code = gen.nextInt(10000);  //random number between 0000 and 9999
		int digits[] = ConvertNumberToArray(code);
		int guess = -1;	
		do{
			guess = PlayerTurn();
			ComputeResults(ConvertNumberToArray(guess, digits));
		}while(guess != code);


	}
	//prompts user for a new guess
	public static int PlayerTurn(){
		System.out.println("Guess the four number code: ");
		Scanner kb = new Scanner(System.in);
		int guess = kb.nextInt();
		while (guess < 0 || guess > 9999){
			System.out.print("Your guess must be between 0000 and 9999");
			System.out.print("\nGuess between 0000 and 9999: ");
			guess = kb.nextInt();
		}
		return guess;
	}
	public static void ComputeResults(int guess, int code){
		int correct = CheckCorrect(guess, code);
		int almost = CheckAlmost(guess, code);
		System.out.printf("Correct: %10d%nAlmost Correct: %3d%n%n", correct, almost);
	}
	public static int CheckCorrect(int[] guess, int code){
		int CodeDigits[] = new int[4];
		for (int i = 3; i >= 0; i--){
			CodeDigits[i] = code%10;
			code /= 10;
		}
		return 0;
	}
	public static int CheckAlmost(int guess, int code){
		return 0;
	}
	public static int[] ConvertNumberToArray(int num){
		int CodeDigits[] = new int[4];
		for (int i = 3; i >= 0; i--){
			CodeDigits[i] = num%10;
			num /= 10;
		}
		return CodeDigits;
	}
}