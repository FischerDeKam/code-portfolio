//Fischer DeKam
//Lab 2
//January 27, 2016
//This Program takes the input for a quadratic equation, and displays the solution(s) to the user
import java.util.Scanner;
public class Lab2 {

	public static void main(String[] args) {
		System.out.println("This program takes the three coefficients of a quadratic equation and finds the solutions.");
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Enter a quadratic equation");
		System.out.println("Ax^2+Bx+C=0");  //If user needs to enter "Ax^2-Bx-C", they should enter a negative. (+-n = -n)
		System.out.println("Type in the quadratic coefficient: A = ?");
		double A = keyboard.nextDouble();
		System.out.println("Type in the linear coefficient: B = ?");
		double B = keyboard.nextDouble();
		System.out.println("Type in the constant: C = ?");
		double C = keyboard.nextDouble();
		
		double Dis = (B*B)-(4*A*C);  //discriminate
		
		if (Dis == 0)  //Discriminate equals 0, one solution
		{
			double Ans = (-B/(2*A));
			System.out.printf("The solution for the quadratic equation %.0fx^2+%.0fx+%.0f is :%.2f", A, B, C, Ans);
		}
		if (Dis > 0)  //Positive Discriminate, two real solutions
		{
			double AnsI = ((-B + Math.sqrt((B*B)-(4*A*C)))/(2*A));
			double AnsII = ((-B - Math.sqrt((B*B)-(4*A*C)))/(2*A));
			System.out.printf("The real solutions for the quadratic equation %.0fx^2+%.0fx+%.0f are: %.2f and %.2f", A, B, C, AnsI, AnsII);
		}
		if (Dis < 0) //Negative Discriminate, two complex solutions
		{
			double AnsI = ((-B/(2*A) + Math.sqrt((-B*B)+(4*A*C)))/(2*A));  //Add complex number to end
			double AnsII = ((-B/(2*A) - Math.sqrt((-B*B)+(4*A*C)))/(2*A)); //Add complex number to end
			System.out.printf("The complex solutions for the quadratic equation %.0fx^2+%.0fx+%.0f are: %.2fi and %.2fi", A, B, C, AnsI, AnsII);
		}
		keyboard.close();
	}

}
