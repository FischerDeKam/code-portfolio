//Fischer DeKam
import java.util.Scanner;

public class Problem_3 {

	public static void main(String[] args) {
		Scanner kb = new Scanner(System.in);
		System.out.println("This Program calculates e^x. Enter x: ");
		int x = kb.nextInt();
		System.out.println("Enter the number of terms to use in the Taylor Series approximation: ");
		int terms = kb.nextInt();
		kb.close();
		double answer = 1;

		for (int i = 1; i < terms; i++){
			if(i == 1){  //when terms = 1, x is added to the answer
				answer += x;
			}
			if (i > 1){
				answer += ((Math.pow(x, i))/(factorial(i))); 
			}

		}

		System.out.println("e^"+x+" ~ "+answer);
	}
	public static double factorial(double terms){  //Factorial
		double a = 1;
		for (int i = 1; i <= (terms); i++){
			a *= i;
		}
		return a;
	}
}