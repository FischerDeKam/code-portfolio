import java.util.Scanner;

/*****************************************************************************
 * Projectile Motion Calculator on a Horizontal Plane
 * (all calculations ignore air resistance)
 * 
 * Written by: Fischer DeKam
 * Fall 2016
 * CS200
 * 
 * Professor: Dean Zeller
 *
 *****************************************************************************/
public class ProjectileMotion {

	public static void main(String[] args) {

		Scanner kb = new Scanner(System.in);
		String response;
		double velocity = 0;
		double distance = 0;
		double theta = 0; 
		double time = 0;
		double velocityY = 0;
		double velocityX = 0;
		double height = 0;
		final double gravity = -9.8;
			
		System.out.println("+-------------------------------------------+");
		System.out.println("|Weclome To The Projectile Motion Calculator|");
		System.out.println("|By: Fischer DeKam                          |");
		System.out.println("+-------------------------------------------+\n");
		
			do{
			System.out.println("+---------Menu-----------+");
			System.out.println("|A - Find Distance Fired |");
			System.out.println("|B - Find Max Height     |");
			System.out.println("|C - Find Travel Time    |");
			System.out.println("|D - Find Launch Angle   |");
			System.out.println("|E - Find Launch Speed   |");
			System.out.println("|F - Display Equations   |");
			System.out.println("|Q - Quit Prorgam        |");
			System.out.println("+------------------------+");
			System.out.println("==>");
			response = kb.next();

			switch(response.toUpperCase()){	
			case "A":
				System.out.println("Finding Distance Fired");
				System.out.println("----------------------\n");
				System.out.println("Enter the launch angle above the horizontal in degrees: ");
				theta = kb.nextDouble();
				System.out.println("Enter the initial launch speed in meters/second: ");
				velocity = kb.nextDouble();

				velocityX = velocity * Math.cos(Math.toRadians(theta)); 
				velocityY = velocity * Math.sin(Math.toRadians(theta));
				time = -velocityY/gravity;
				distance = velocityX * 2 * time;

				System.out.println("The object travels "+distance+" meters in the horizontal direction.\n\n");
				break;

			case "B":
				System.out.println("Finding Max Height");
				System.out.println("------------------\n");
				System.out.println("Enter the launch angle above the horizontal in degrees: ");
				theta = kb.nextDouble();
				System.out.println("Enter the initial launch speed in meters/second: ");
				velocity = kb.nextDouble();

				velocityY = velocity * Math.sin(Math.toRadians(theta));
				height = -(velocityY * velocityY)/(2*gravity);

				System.out.println("The object reaches a max height of "+height+" meters.\n\n");
				break;

			case "C":
				System.out.println("Finding travel time");
				System.out.println("-------------------\n");
				System.out.println("Enter the launch angle above the horizontal in degrees: ");
				theta = kb.nextDouble();
				System.out.println("Enter the initial launch speed in meters/second: ");
				velocity = kb.nextDouble();

				velocityY = velocity * Math.sin(Math.toRadians(theta));
				time = -velocityY/gravity;

				System.out.println("The total time the object is in the air is "+(2*time)+" seconds.");
				System.out.println("The time to reach it's max height is "+time+" seconds.\n\n");
				break;

			case "D":
				System.out.println("Finding launch angle");
				System.out.println("--------------------\n");
				System.out.println("Enter the total distance traveled in the horizontal direction: ");
				distance = kb.nextDouble();
				System.out.println("Enter the initial launch speed in meters/second: ");
				velocity = kb.nextDouble();
				
				theta = 0.5*(Math.asin((-gravity*distance)/(velocity*velocity))*180/Math.PI);
				
				System.out.println("The launch angle is "+theta+" degrees above the horizontal.\n\n");
				break;

			case "E":
				System.out.println("Finding initial launch velocity");
				System.out.println("-------------------------------\n");
				System.out.println("Enter the total distance traveled in the horizontal direction: ");
				distance = kb.nextDouble();
				System.out.println("Enter the launch angle above the horizontal in degrees: ");
				theta = kb.nextDouble();
				
				velocity = Math.sqrt((distance*gravity)/(Math.toRadians(Math.sin(theta))));
				
				System.out.println("The initial launch velocity is "+velocity+" meters per second.\n\n");
				break;

			case "F":
				System.out.println("Kinematic Equations");
				System.out.println("-------------------\n");
				System.out.println("Vf = Vi + (a)(delta t)");
				System.out.println("Vf^2 = Vi^2 + (2)(a)(delta t)^2");
				System.out.println("Xf = Xi + (Vi)(delta t)+(1/2)(a)(delta t)^2");
				System.out.println("v = (delta x)/(delta t)");
				System.out.println("a = (delta v)/(delta t)");
				System.out.println("Acceleration due to gravity = -9.8m/s^2\n\n");
				break;
			}
		}while (!response.equalsIgnoreCase("Q"));
		kb.close();
		System.out.println("Thank you for using the program!");
	}

}
